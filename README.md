# Computer Controlled Loom

This repository contains all the working files from my experiments building a home brew Jaquard Loom. The loom is driven by an 
iPhone app using Bluetooth. The repository has all the source code, design files, etc.  However it's important to understand that this is 
not a how-to or a kit.  It's just a raw look into at a hobbby project, so no effort has gone into documenting what's in the design files, 
or how to turn them into a functional loom. The loom does work but was somewhat fiddly to get working, and is pretty slow to operate.  

If you are someone who is comfortable using a 3D printer, laser cutter, building iPhone apps, and you're ok with populating and reflowing 
SMD PCBs then this may act as a good jumping off point for you.

Feel free to ask me questions. There is also a [video](https://www.youtube.com/watch?v=Czun6yVpzp0) about the loom, and a [blog post](https://retrotechjournal.com/2019/06/17/making-a-diy-computer-controlled-loom/).

![Loom](docs/loomStrungUp.jpg)

## Basic Pieces

There is the WeaveMe iPhone app, a PCB designed in Eagle, Firmware written using the Arduino IDE, Laser cutter vector files done in 
Illustrator, and a big Rhino 3D file containing all the 3D printed parts of the loom. 

### Tools I Used

XCode, Arduino IDE, Eagle, Rhino 3D, Illustrator, Slic3r 3D

## Built With

* [XCode](https://developer.apple.com/xcode/) - To build the iPhone app
* [Arduino IDE](https://www.arduino.cc/en/Main/Software) - To build the firmware
* [MightyCore](https://github.com/MCUdude/MightyCore) - The Arduino hardware package to support ATMEGA644
(be sure to use the Bobuino pinout)
* [Adafruit Neopixel](https://github.com/adafruit/Adafruit_NeoPixel) - To drive the leds
* [AccelStepper](http://www.airspayce.com/mikem/arduino/AccelStepper/) - To drive the stepper motors

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* The gear icon, and the clockwise/counter clockwise arrow icons came from [icons8](https://icons8.com)

