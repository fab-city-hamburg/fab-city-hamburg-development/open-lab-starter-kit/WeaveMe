//
//  PatternCell.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/13/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import UIKit

class PatternCell: UICollectionViewCell {
    
    static var reuseIdentifier = "PatternCell"

    // You must set this before setting the pattern.
    var patternRenderer: PatternRenderer?

    var pattern: Pattern? {
        didSet {
            guard let pattern = pattern else {
                titleLabel.text = ""
                subtitleLabel.text = ""
                colorSwatchView.colors = []
                thumbnailImageView.image = nil
                return
            }
            
            titleLabel.text = pattern.name
            
            if let width = pattern.threadCount(.warp),
                let height = pattern.threadCount(.weft) {
                let layerCount = pattern.patternLayers.count
                if layerCount == 1 {
                    subtitleLabel.text = "1 layer   \(width)x\(height) "
                } else {
                    subtitleLabel.text = "\(layerCount) layers   \(width)x\(height) "
                }
            } else {
                subtitleLabel.text = "no layers"
            }

            if pattern.thumbnailImage != nil {
                thumbnailImageView.image = pattern.thumbnailImage
            } else {
                if let renderer = patternRenderer,
                    let width = pattern.threadCount(.warp),
                    let height = pattern.threadCount(.weft) {
                    let rect = getRectForThumbnail(width: width, height: height)
                    renderer.render(pattern: pattern, style: .normalWarpAndWeft, rect: rect, targetOutputWidth: thumbnailImageView.bounds.size.width) { image, error in
                        if let image = image {

                            // If the cell has been re-used while we were rendering, we cache the image with the
                            // pattern, but don't set the thumbnailImage.
                            pattern.thumbnailImage = image
                            // TODO: Figure out why the == doesn't work with patterns.  Implement isEqual or whatever.
                            if self.pattern?.name != nil && self.pattern?.name == pattern.name {
                                self.thumbnailImageView.image = image
                            }
                        }
                    }
                } else {
                    thumbnailImageView.image = nil
                }
            }

            colorSwatchView.colors = pattern.uniqueColors()
        }
    }
    
    // For now, if the pattern is taller than it is wide and it's fairly big, we
    // crop to the bottom of the image so the thumbnail shows what they'd see
    // when they start weaving the pattern.  Eventually if these patterns come from
    // an authoring tool it could specify it's own thumbnail region.
    private func getRectForThumbnail(width: Int, height: Int) -> CGRect {
        if height < 20 || width >= height {
            return CGRect(x: 0, y: 0, width: width, height: height)
        }
        return CGRect(x: 0, y: height - width, width: width, height: width)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        pattern = nil
    }
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var colorSwatchView: ColorSwatchView!
    
}
