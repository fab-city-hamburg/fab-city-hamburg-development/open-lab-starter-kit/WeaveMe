//
//  LayerBuildItem.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/30/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  Is is the set of information that a layer builder returns to a layer.  The main thing
//  is warpOnTop, but various pieces of color info are also returned.

import UIKit

struct LayerBuildItem {

    // 2D Array of is the weft on top in that part of the pattern.
    var weftOnTop: [[Bool]] = []

    // Arrays of colors for the warp/weft.
    var warpColors: [UIColor] = []
    var weftColors: [UIColor] = []

}
