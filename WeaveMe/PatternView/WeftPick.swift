//
//  WeftPick.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/4/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import UIKit

class WeftPick: NSObject {
    var threadColor: UIColor = .clear
    var threadDiameter: CGFloat = 1.0
    
    var y: Int
    var minX: Int
    var maxX: Int
    
    var direction = PickDirection.leftToRight
    
    enum LeverType {
        case positive
        case negative
    }
    
    var leverType: LeverType = .positive
    
    var patternLayer: PatternLayer
    
    init(patternLayer: PatternLayer, threadColor: UIColor, threadDiameter: CGFloat, direction: PickDirection, leverType: LeverType, y: Int,
         minX: Int, maxX: Int) {
        self.patternLayer = patternLayer
        self.threadColor = threadColor
        self.threadDiameter = threadDiameter
        self.direction = direction
        self.leverType = leverType
        self.y = y
        self.minX = minX
        self.maxX = maxX
    }
    
    var rect: CGRect {
        return CGRect.init(x: minX, y: y, width: (maxX - minX) + 1, height: 1)
    }
    
    // If you want to see if two picks are using "the same" thread you can use this.
    // They aren't for sure part of the same shuttle unless they are on the same layer.
    var threadSignature: String {
        return ":\(threadDiameter)" + threadColor.hexString()
    }
}
