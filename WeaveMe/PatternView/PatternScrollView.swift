//
//  PatternScrollView.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/23/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This is a scrolling view for exploring a pattern.  Right now it only
//  supports a few internal image views that are the full size of the pattern, but
//  for big complex patterns this may eventually handle things like incrementally
//  rendering sections, allowing zooming, etc.
//
//  This also has the ability to highlight various parts of a pattern to help users
//  step though the various picks needed to weave the pattern.
//

import UIKit

class PatternScrollView: UIScrollView {

    var pattern: Pattern? {
        didSet {
            if let newPattern = pattern {
                patternLayerViewCoordinator = PatternLayerViewScrollCoordinator(pattern: newPattern, scrollView: self)
            }
        }
    }

    private var patternLayerViewCoordinator: PatternLayerViewScrollCoordinator?

    private var setupInProgress = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // Kurt you have two improvments left to try:  1 make it so cached images hold the scale, so all the thin warps
        // don't cause a ton of extra work and are pixel aligned.
        // 2 see about sharing the cache across multiple renderers.
        if let coordinator = patternLayerViewCoordinator {
            if !coordinator.hasBeenSetUp && !setupInProgress {
                setupInProgress = true
                NSLog("Starting renders")
                let startDate = Date()
                coordinator.setupAndRenderVisible(animated: true) { (error) in
                    NSLog("Visible done \(String(format: "%.2f", Date().timeIntervalSince(startDate)))")
                    let midDate = Date()
                    coordinator.renderYetToBeRenderedViews() { (error) in
                        NSLog("Non Visible done \(String(format: "%.2f", Date().timeIntervalSince(midDate)))")
                        NSLog("Total \(String(format: "%.2f", Date().timeIntervalSince(startDate)))")
                        self.setupInProgress = false
                    }
                }
            }
        }
    }

}
